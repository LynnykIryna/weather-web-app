window.addEventListener('load', () => {

    let long;
    let lat;
    const weatherDescription = document.querySelector('.weather-description');
    const degree = document.querySelector('.degree');
    const location = document.querySelector('.location-name');
    const weatherIcon = document.querySelector('.weather-icon');
    const temperatureUnit = document.querySelector('.temperature-unit');
    const degreeWrap = document.querySelector('.degree-wrapping');
    const modal = document.querySelector('.modal');
    const wrapper = document.querySelector('.wrapper');
    const searchInput = document.querySelector('.search-input');
    const searchBtn = document.querySelector('.search-button');
    const cancelBtn = document.querySelector('.cancel-button');
    const humidity = document.querySelector('.humidity');
    const humidityUnit = document.querySelector('.humidity-unit');

    //initial determination of the users location by browsers geolocation
    if (!navigator.geolocation) {
        alert(`Your browser doesn't support Geolocation`);
    }
    navigator.geolocation.getCurrentPosition(onSuccess, onError);

    //when the city name is pressed user can choose another
    location.addEventListener('click', (e) => {
        e.preventDefault();
        addModal();
    });

    //cancel search
    cancelBtn.addEventListener('click', hideModal)

    //when the search button is pressed
    searchBtn.addEventListener('click', (e) => {
        //cancel a loader
        e.preventDefault()

        const pattern = /^[aA-zZ][\saA-zZ-]+[a-z]$/;
        let cityName;
        if (isEmpty(searchInput.value) === true) {
            return alert('Please, enter city name');
        }

        if (!pattern.test(cityName)) {
            return alert('Incorrect city name');
        }


        cityName = replaceSpaces(searchInput.value);
        const searchAPI = `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=f8000e7f8241fe3215362a825811a67b`;
        getInfoFromAPI(searchAPI);
        hideModal()
    })


    function onSuccess(position) {

        long = position.coords.longitude;
        lat = position.coords.latitude;

        const api = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=f8000e7f8241fe3215362a825811a67b`;

        getInfoFromAPI(api);
    }

    function onError() {
        alert('Sorry, we can not determine your location. Please, choose your city');
        addModal();
    }

    function getInfoFromAPI(api) {
        fetch(api)
            .then(response => {
                if (response.ok) {
                    console.log(`Respose status: ${response.statusText}`);
                    return response.json();
                }
                let error = new Error(response.statusText);
                error.response = response;
                throw error

            })
            .then(data => {
                console.log(data);
                if (data == undefined) {
                    console.log('Error: ' + 'data undefined');
                    alert('Something went wrong')
                    return false
                }
                //hide preloader
                hidePreloader();

                //set DOM elem from API
                setDOMelements(data);

                //changes temperature unit
                degreeWrap.addEventListener('click', () => changeTemperatureUnit(data))
            })
            .catch((e) => {
                console.log('Error: ' + e.message);
                console.log(e.response);
                alert('Something went wrong')
            });
    }

    function setDOMelements(data) {
        if (!data.hasOwnProperty('name') || isEmpty(data.name) === true || !data.hasOwnProperty('main')) {
            console.log('Error: ' + 'data has no required properties');
            return alert('Something went wrong');
        }
        changeTemperatureUnit(data);

        location.textContent = data.name;

        if (data.weather[0].hasOwnProperty('description') && !isEmpty(data.weather[0].description)) {
            weatherDescription.textContent = data.weather[0].description[0].toUpperCase() +
                data.weather[0].description.substr(1);
        }
        if (data.main.hasOwnProperty('humidity') && !isEmpty(data.main.humidity.toString())) {
            humidity.textContent = data.main.humidity;
            humidityUnit.textContent = '%';
        }
        if (data.weather[0].hasOwnProperty('icon') && !isEmpty(data.weather[0].icon)) {
            weatherIcon.setAttribute('src', ` http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`)
        }
    }

    function changeTemperatureUnit(data) {
        if (!data.main.hasOwnProperty('temp') || isEmpty(data.main.temp.toString()) === true) {
            console.log('Error: ' + 'data has no temperature property');
            return alert('Something went wrong');
        }

        const kelvin = data.main.temp;
        const fahrenheit = Math.round(1.8 * (kelvin - 273) + 32);
        const celsius = Math.round(kelvin - 273);

        switch (temperatureUnit.textContent) {
            case 'K':
                temperatureUnit.textContent = 'F';
                degree.textContent = fahrenheit;
                break;
            case 'F':
                temperatureUnit.textContent = 'C';
                degree.textContent = celsius;
                break;
            case 'C':
                temperatureUnit.textContent = 'K'
                degree.textContent = kelvin;
        }
    }

    function addModal() {
        wrapper.classList.add('shadow');
        modal.classList.remove('disable');
        modal.classList.add('active');
    }

    function hideModal() {
        wrapper.classList.remove('shadow');
        modal.classList.add('disable');
        modal.classList.remove('active');
    }

    function hidePreloader() {
        const delayForAttenuation = 500;
        document.body.classList.add('loaded_hiding');
        window.setTimeout(function () {
            document.body.classList.add('loaded');
            document.body.classList.remove('loaded_hiding');
        }, delayForAttenuation);
    }

    function replaceSpaces(str) {
        str = str.replace(/\s+/g, ' ') //removes long spaces
            .trim() //removes space at the beginning and end
        return str;
    }

    function isEmpty(str) {
        return str.trim().length === 0;
    }
})
